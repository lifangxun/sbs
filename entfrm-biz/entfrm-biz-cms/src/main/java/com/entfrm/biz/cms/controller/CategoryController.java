package com.entfrm.biz.cms.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
    import com.entfrm.biz.cms.entity.Category;
import com.entfrm.biz.cms.service.CategoryService;
import com.entfrm.core.log.annotation.OperLog;
import org.springframework.security.access.prepost.PreAuthorize;
import com.entfrm.core.base.api.R;
import com.entfrm.core.base.util.ExcelUtil;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Arrays;

/**
 * @author entfrm
 * @date 2020-09-24 10:44:26
 *
 * @description 类别Controller
 */
@Api("类别管理")
@RestController
@AllArgsConstructor
@RequestMapping("/cms/category")
public class CategoryController {

    private final CategoryService categoryService;

    private QueryWrapper<Category> getQueryWrapper(Category category) {
            return new QueryWrapper<Category>()
                                                            .like(StrUtil.isNotBlank(category.getName()), "name", category.getName())
                                                                                                                .eq(StrUtil.isNotBlank(category.getStatus()), "status", category.getStatus())
                                                                                                                                                    .orderByDesc("create_time");
    }

            @ApiOperation("类别列表")
        @PreAuthorize("@ps.hasPerm('category_view')")
        @GetMapping("/list")
        public R list(Category category) {
            List<Category> categoryList = categoryService.list(getQueryWrapper(category));
            return R.ok(categoryList);
        }
    
    @ApiOperation("类别查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") Integer id) {
        return R.ok(categoryService.getById(id));
    }

    @OperLog("类别新增")
    @ApiOperation("类别新增")
    @PreAuthorize("@ps.hasPerm('category_add')")
    @PostMapping("/save")
    public R save(@Validated @RequestBody Category category) {
            categoryService.save(category);
        return R.ok();
    }

    @OperLog("类别修改")
    @ApiOperation("类别修改")
    @PreAuthorize("@ps.hasPerm('category_edit')")
    @PutMapping("/update")
    public R update(@Validated @RequestBody Category category) {
            categoryService.updateById(category);
        return R.ok();
    }


    @OperLog("类别删除")
    @ApiOperation("类别删除")
    @PreAuthorize("@ps.hasPerm('category_del')")
    @DeleteMapping("/remove/{id}")
    public R remove(@PathVariable("id") Integer[] id) {
        return R.ok(categoryService.removeByIds(Arrays.asList(id)));
    }


            @GetMapping("/categoryTree")
        public R categoryTree() {
            List<Category> categoryList = categoryService.list(new QueryWrapper<Category>().orderByAsc("sort"));
            return R.ok(categoryService.buildTree(categoryList, 0));
        }
    
    @PreAuthorize("@ps.hasPerm('category_export')")
    @GetMapping("/export")
    public R export(Category category) {
        List<Category> list = categoryService.list(getQueryWrapper(category));
        ExcelUtil<Category> util = new ExcelUtil<Category>(Category. class);
        return util.exportExcel(list, "类别数据");
    }
}
