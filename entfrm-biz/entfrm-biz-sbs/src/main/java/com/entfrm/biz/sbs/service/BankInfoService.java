package com.entfrm.biz.sbs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.sbs.entity.BankInfo;

/**
 * @author entfrm
 * @date 2021-02-23 23:17:49
 *
 * @description 银行列表Service接口
 */
public interface BankInfoService extends IService<BankInfo> {
    }
