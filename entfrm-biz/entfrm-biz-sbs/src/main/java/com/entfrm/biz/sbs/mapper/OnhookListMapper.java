package com.entfrm.biz.sbs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.sbs.entity.OnhookList;

/**
 * @author lfx&amp;yy
 * @date 2021-01-30 15:06:04
 *
 * @description 挂机列表Mapper接口
 */
public interface OnhookListMapper extends BaseMapper<OnhookList>{

}
