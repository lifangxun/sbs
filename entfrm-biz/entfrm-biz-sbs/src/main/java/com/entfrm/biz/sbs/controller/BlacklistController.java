package com.entfrm.biz.sbs.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
        import com.baomidou.mybatisplus.core.metadata.IPage;
    import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
    import com.entfrm.biz.sbs.entity.Blacklist;
import com.entfrm.biz.sbs.service.BlacklistService;
import com.entfrm.core.log.annotation.OperLog;
import org.springframework.security.access.prepost.PreAuthorize;
import com.entfrm.core.base.api.R;
import com.entfrm.core.base.util.ExcelUtil;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Arrays;

/**
 * @author entfrm
 * @date 2021-02-22 22:33:05
 *
 * @description 黑名单管理Controller
 */
@Api("黑名单管理管理")
@RestController
@AllArgsConstructor
@RequestMapping("/sbs/blacklist")
public class BlacklistController {

    private final BlacklistService blacklistService;

    private QueryWrapper<Blacklist> getQueryWrapper(Blacklist blacklist) {
            return new QueryWrapper<Blacklist>()
                                                        .eq(!StrUtil.isEmptyIfStr(blacklist.getUserId()), "user_id", blacklist.getUserId())
                                                .eq(!StrUtil.isEmptyIfStr(blacklist.getConnectId()), "connect_id", blacklist.getConnectId())
                                                                                                                                                    .orderByDesc("create_time");
    }

            @ApiOperation("黑名单管理列表")
        @PreAuthorize("@ps.hasPerm('blacklist_view')")
        @GetMapping("/list")
        public R list(Page page, Blacklist blacklist) {
            IPage<Blacklist> blacklistPage = blacklistService.page(page, getQueryWrapper(blacklist));
            return R.ok(blacklistPage.getRecords(), blacklistPage.getTotal());
        }
    
    @ApiOperation("黑名单管理查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") Integer id) {
        return R.ok(blacklistService.getById(id));
    }

    @OperLog("黑名单管理新增")
    @ApiOperation("黑名单管理新增")
    @PreAuthorize("@ps.hasPerm('blacklist_add')")
    @PostMapping("/save")
    public R save(@Validated @RequestBody Blacklist blacklist) {
            blacklistService.save(blacklist);
        return R.ok();
    }

    @OperLog("黑名单管理修改")
    @ApiOperation("黑名单管理修改")
    @PreAuthorize("@ps.hasPerm('blacklist_edit')")
    @PutMapping("/update")
    public R update(@Validated @RequestBody Blacklist blacklist) {
            blacklistService.updateById(blacklist);
        return R.ok();
    }


    @OperLog("黑名单管理删除")
    @ApiOperation("黑名单管理删除")
    @PreAuthorize("@ps.hasPerm('blacklist_del')")
    @DeleteMapping("/remove/{id}")
    public R remove(@PathVariable("id") Integer[] id) {
        return R.ok(blacklistService.removeByIds(Arrays.asList(id)));
    }


    
    @PreAuthorize("@ps.hasPerm('blacklist_export')")
    @GetMapping("/export")
    public R export(Blacklist blacklist) {
        List<Blacklist> list = blacklistService.list(getQueryWrapper(blacklist));
        ExcelUtil<Blacklist> util = new ExcelUtil<Blacklist>(Blacklist. class);
        return util.exportExcel(list, "黑名单管理数据");
    }
}
