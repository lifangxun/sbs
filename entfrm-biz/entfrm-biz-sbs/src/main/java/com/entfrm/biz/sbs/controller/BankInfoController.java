package com.entfrm.biz.sbs.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
        import com.baomidou.mybatisplus.core.metadata.IPage;
    import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
    import com.entfrm.biz.sbs.entity.BankInfo;
import com.entfrm.biz.sbs.service.BankInfoService;
import com.entfrm.core.log.annotation.OperLog;
import org.springframework.security.access.prepost.PreAuthorize;
import com.entfrm.core.base.api.R;
import com.entfrm.core.base.util.ExcelUtil;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Arrays;

/**
 * @author entfrm
 * @date 2021-02-23 23:17:49
 *
 * @description 银行列表Controller
 */
@Api("银行列表管理")
@RestController
@AllArgsConstructor
@RequestMapping("/sbs/bankInfo")
public class BankInfoController {

    private final BankInfoService bankInfoService;

    private QueryWrapper<BankInfo> getQueryWrapper(BankInfo bankInfo) {
            return new QueryWrapper<BankInfo>()
                                                            .like(StrUtil.isNotBlank(bankInfo.getAccountName()), "account_name", bankInfo.getAccountName())
                                                    .eq(StrUtil.isNotBlank(bankInfo.getAccountType()), "account_type", bankInfo.getAccountType())
                                                .eq(!StrUtil.isEmptyIfStr(bankInfo.getUserId()), "user_id", bankInfo.getUserId())
                                                    .eq(StrUtil.isNotBlank(bankInfo.getCardNo()), "card_no", bankInfo.getCardNo())
                                                    .eq(StrUtil.isNotBlank(bankInfo.getBankType()), "bank_type", bankInfo.getBankType())
                                                    .eq(StrUtil.isNotBlank(bankInfo.getBankAddress()), "bank_address", bankInfo.getBankAddress())
                                                    .eq(StrUtil.isNotBlank(bankInfo.getPhone()), "phone", bankInfo.getPhone())
                                                    .eq(StrUtil.isNotBlank(bankInfo.getQq()), "qq", bankInfo.getQq())
                                                                                                                                                    .orderByDesc("create_time");
    }

            @ApiOperation("银行列表列表")
        @PreAuthorize("@ps.hasPerm('bankInfo_view')")
        @GetMapping("/list")
        public R list(Page page, BankInfo bankInfo) {
            IPage<BankInfo> bankInfoPage = bankInfoService.page(page, getQueryWrapper(bankInfo));
            return R.ok(bankInfoPage.getRecords(), bankInfoPage.getTotal());
        }
    
    @ApiOperation("银行列表查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") Integer id) {
        return R.ok(bankInfoService.getById(id));
    }

    @OperLog("银行列表新增")
    @ApiOperation("银行列表新增")
    @PreAuthorize("@ps.hasPerm('bankInfo_add')")
    @PostMapping("/save")
    public R save(@Validated @RequestBody BankInfo bankInfo) {
            bankInfoService.save(bankInfo);
        return R.ok();
    }

    @OperLog("银行列表修改")
    @ApiOperation("银行列表修改")
    @PreAuthorize("@ps.hasPerm('bankInfo_edit')")
    @PutMapping("/update")
    public R update(@Validated @RequestBody BankInfo bankInfo) {
            bankInfoService.updateById(bankInfo);
        return R.ok();
    }


    @OperLog("银行列表删除")
    @ApiOperation("银行列表删除")
    @PreAuthorize("@ps.hasPerm('bankInfo_del')")
    @DeleteMapping("/remove/{id}")
    public R remove(@PathVariable("id") Integer[] id) {
        return R.ok(bankInfoService.removeByIds(Arrays.asList(id)));
    }


    
    @PreAuthorize("@ps.hasPerm('bankInfo_export')")
    @GetMapping("/export")
    public R export(BankInfo bankInfo) {
        List<BankInfo> list = bankInfoService.list(getQueryWrapper(bankInfo));
        ExcelUtil<BankInfo> util = new ExcelUtil<BankInfo>(BankInfo. class);
        return util.exportExcel(list, "银行列表数据");
    }
}
