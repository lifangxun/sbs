package com.entfrm.biz.sbs.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
        import com.baomidou.mybatisplus.core.metadata.IPage;
    import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
    import com.entfrm.biz.sbs.entity.Trumpet;
import com.entfrm.biz.sbs.service.TrumpetService;
import com.entfrm.core.log.annotation.OperLog;
import org.springframework.security.access.prepost.PreAuthorize;
import com.entfrm.core.base.api.R;
import com.entfrm.core.base.util.ExcelUtil;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Arrays;

/**
 * @author entfrm
 * @date 2021-02-22 23:16:44
 *
 * @description 小号管理Controller
 */
@Api("小号管理管理")
@RestController
@AllArgsConstructor
@RequestMapping("/sbs/trumpet")
public class TrumpetController {

    private final TrumpetService trumpetService;

    private QueryWrapper<Trumpet> getQueryWrapper(Trumpet trumpet) {
            return new QueryWrapper<Trumpet>()
                                                        .eq(!StrUtil.isEmptyIfStr(trumpet.getUserId()), "user_id", trumpet.getUserId())
                                                    .like(StrUtil.isNotBlank(trumpet.getTrumpetName()), "trumpet_name", trumpet.getTrumpetName())
                                                    .eq(StrUtil.isNotBlank(trumpet.getTrumpetPwd()), "trumpet_pwd", trumpet.getTrumpetPwd())
                                                    .eq(StrUtil.isNotBlank(trumpet.getTrumpetLevel()), "trumpet_level", trumpet.getTrumpetLevel())
                                                    .eq(StrUtil.isNotBlank(trumpet.getTrumpetPlateform()), "trumpet_plateform", trumpet.getTrumpetPlateform())
                                                    .eq(StrUtil.isNotBlank(trumpet.getConsumeLevel()), "consume_level", trumpet.getConsumeLevel())
                                                    .eq(StrUtil.isNotBlank(trumpet.getAddress()), "address", trumpet.getAddress())
                                                    .eq(StrUtil.isNotBlank(trumpet.getSex()), "sex", trumpet.getSex())
                                                    .eq(StrUtil.isNotBlank(trumpet.getAge()), "age", trumpet.getAge())
                                                    .like(StrUtil.isNotBlank(trumpet.getZfbName()), "zfb_name", trumpet.getZfbName())
                                                    .eq(StrUtil.isNotBlank(trumpet.getZfbAccount()), "zfb_account", trumpet.getZfbAccount())
                                                    .like(StrUtil.isNotBlank(trumpet.getReceiveName()), "receive_name", trumpet.getReceiveName())
                                                    .eq(StrUtil.isNotBlank(trumpet.getReceivePhone()), "receive_phone", trumpet.getReceivePhone())
                                                    .eq(StrUtil.isNotBlank(trumpet.getReceiveAddress()), "receive_address", trumpet.getReceiveAddress())
                                                    .like(StrUtil.isNotBlank(trumpet.getIsRealname()), "is_realname", trumpet.getIsRealname())
                                                    .eq(StrUtil.isNotBlank(trumpet.getIsRealphone()), "is_realphone", trumpet.getIsRealphone())
                                                    .eq(StrUtil.isNotBlank(trumpet.getIsOn()), "is_on", trumpet.getIsOn())
                                                    .eq(StrUtil.isNotBlank(trumpet.getIsAudit()), "is_audit", trumpet.getIsAudit())
                                                    .eq(StrUtil.isNotBlank(trumpet.getMaxOrderNo()), "max_order_no", trumpet.getMaxOrderNo())
                                                    .eq(StrUtil.isNotBlank(trumpet.getMaxMoney()), "max_money", trumpet.getMaxMoney())
                                                    .eq(StrUtil.isNotBlank(trumpet.getStatus()), "status", trumpet.getStatus())
                                                                                                                                                    .orderByDesc("create_time");
    }

            @ApiOperation("小号管理列表")
        @PreAuthorize("@ps.hasPerm('trumpet_view')")
        @GetMapping("/list")
        public R list(Page page, Trumpet trumpet) {
            IPage<Trumpet> trumpetPage = trumpetService.page(page, getQueryWrapper(trumpet));
            return R.ok(trumpetPage.getRecords(), trumpetPage.getTotal());
        }
    
    @ApiOperation("小号管理查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") Integer id) {
        return R.ok(trumpetService.getById(id));
    }

    @OperLog("小号管理新增")
    @ApiOperation("小号管理新增")
    @PreAuthorize("@ps.hasPerm('trumpet_add')")
    @PostMapping("/save")
    public R save(@Validated @RequestBody Trumpet trumpet) {
            trumpetService.save(trumpet);
        return R.ok();
    }

    @OperLog("小号管理修改")
    @ApiOperation("小号管理修改")
    @PreAuthorize("@ps.hasPerm('trumpet_edit')")
    @PutMapping("/update")
    public R update(@Validated @RequestBody Trumpet trumpet) {
            trumpetService.updateById(trumpet);
        return R.ok();
    }


    @OperLog("小号管理删除")
    @ApiOperation("小号管理删除")
    @PreAuthorize("@ps.hasPerm('trumpet_del')")
    @DeleteMapping("/remove/{id}")
    public R remove(@PathVariable("id") Integer[] id) {
        return R.ok(trumpetService.removeByIds(Arrays.asList(id)));
    }


    
    @PreAuthorize("@ps.hasPerm('trumpet_export')")
    @GetMapping("/export")
    public R export(Trumpet trumpet) {
        List<Trumpet> list = trumpetService.list(getQueryWrapper(trumpet));
        ExcelUtil<Trumpet> util = new ExcelUtil<Trumpet>(Trumpet. class);
        return util.exportExcel(list, "小号管理数据");
    }
}
