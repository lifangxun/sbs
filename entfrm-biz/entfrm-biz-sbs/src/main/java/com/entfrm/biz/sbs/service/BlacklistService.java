package com.entfrm.biz.sbs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.sbs.entity.Blacklist;

/**
 * @author entfrm
 * @date 2021-02-22 22:33:05
 *
 * @description 黑名单管理Service接口
 */
public interface BlacklistService extends IService<Blacklist> {
    }
