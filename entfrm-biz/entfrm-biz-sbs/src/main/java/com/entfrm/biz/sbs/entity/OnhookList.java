package com.entfrm.biz.sbs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.entfrm.core.base.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.entfrm.core.data.entity.BaseEntity;

/**
 * @author lfx&amp;yy
 * @date 2021-01-30 15:06:04
 *
 * @description 挂机列表对象 OnhookList
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sbs_onhook_list")
public class OnhookList extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /**  */
    @TableId
    private Integer id;

    /** 分区（淘宝，京东） */
    @Excel(name = "分区", convertExp = "淘=宝，京东")
    private String hookPartition;

    /** 会员信息 */
    @Excel(name = "会员信息")
    private Integer userId;

    /** 淘宝小号（名称，等级，消费高低） */
    @Excel(name = "淘宝小号", convertExp = "名=称，等级，消费高低")
    private String tbTrumpet;

    /** 等级（一钻，两钻） */
    @Excel(name = "等级", convertExp = "一=钻，两钻")
    private String starLevel;

    /** 消费水平 */
    @Excel(name = "消费水平")
    private String consumeLevel;

    /** 日刷 */
    @Excel(name = "日刷")
    private Integer dayBrush;

    /** 我刷 */
    @Excel(name = "我刷")
    private Integer myBrush;

    /** 状态（连接，空闲） */
    @Excel(name = "状态", convertExp = "连=接，空闲")
    private String status;

    /** 标签 */
    @Excel(name = "标签")
    private String tag;

    /** 上限金额 */
    @Excel(name = "上限金额")
    private String maxMoney;

    /** 垫付 */
    @Excel(name = "垫付")
    private String advancePay;

    /** 地区 */
    @Excel(name = "地区")
    private String address;

    /** 网络 */
    @Excel(name = "网络")
    private String network;


}
