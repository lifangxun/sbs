package com.entfrm.biz.sbs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.entfrm.core.base.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.entfrm.core.data.entity.BaseEntity;

/**
 * @author entfrm
 * @date 2021-02-22 22:33:05
 *
 * @description 黑名单管理对象 Blacklist
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sbs_blacklist")
public class Blacklist extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /**  */
    @TableId
    private Integer id;

    /** 用户名 */
    @Excel(name = "用户名")
    private Integer userId;

    /** 黑名单连接id */
    @Excel(name = "黑名单连接id")
    private Integer connectId;


}
