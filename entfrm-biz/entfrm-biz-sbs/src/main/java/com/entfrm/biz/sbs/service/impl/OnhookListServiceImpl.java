package com.entfrm.biz.sbs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
    import org.springframework.stereotype.Service;
import com.entfrm.biz.sbs.mapper.OnhookListMapper;
import com.entfrm.biz.sbs.entity.OnhookList;
import com.entfrm.biz.sbs.service.OnhookListService;

/**
 * @author lfx&amp;yy
 * @date 2021-01-30 15:06:04
 *
 * @description 挂机列表Service业务层
 */
@Service
public class OnhookListServiceImpl extends ServiceImpl<OnhookListMapper, OnhookList> implements OnhookListService {
    }
