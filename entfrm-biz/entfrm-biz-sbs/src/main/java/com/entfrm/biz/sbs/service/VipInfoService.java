package com.entfrm.biz.sbs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.sbs.entity.VipInfo;

/**
 * @author yy
 * @date 2021-01-30 15:06:27
 *
 * @description 会员信息Service接口
 */
public interface VipInfoService extends IService<VipInfo> {
    }
