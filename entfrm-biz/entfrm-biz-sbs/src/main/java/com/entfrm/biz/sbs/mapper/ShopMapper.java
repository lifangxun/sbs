package com.entfrm.biz.sbs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.sbs.entity.Shop;

/**
 * @author entfrm
 * @date 2021-02-22 22:30:42
 *
 * @description 店铺管理Mapper接口
 */
public interface ShopMapper extends BaseMapper<Shop>{

}
