package com.entfrm.biz.sbs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
    import org.springframework.stereotype.Service;
import com.entfrm.biz.sbs.mapper.ConnectMapper;
import com.entfrm.biz.sbs.entity.Connect;
import com.entfrm.biz.sbs.service.ConnectService;

/**
 * @author entfrm
 * @date 2021-02-22 22:31:12
 *
 * @description 连接记录Service业务层
 */
@Service
public class ConnectServiceImpl extends ServiceImpl<ConnectMapper, Connect> implements ConnectService {
    }
