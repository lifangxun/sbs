package com.entfrm.biz.sbs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.entfrm.core.base.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.entfrm.core.data.entity.BaseEntity;
import java.util.Date;

/**
 * @author yy
 * @date 2021-01-30 15:06:27
 *
 * @description 会员信息对象 VipInfo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sbs_vip_info")
public class VipInfo extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /**  */
    @TableId
    private Integer id;

    /** 账号id */
    @Excel(name = "账号id")
    private Integer userId;

    /** 用户名 */
    @Excel(name = "用户名")
    private String userName;

    /** 用户类型 */
    @Excel(name = "用户类型")
    private String vipType;

    /** 到期时间 */
    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /** 余额 */
    @Excel(name = "余额")
    private String balance;

    /** 金币 */
    @Excel(name = "金币")
    private String goldCoin;

    /** 佣金 */
    @Excel(name = "佣金")
    private String commission;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;


}
