package com.entfrm.biz.sbs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.sbs.entity.Shop;

/**
 * @author entfrm
 * @date 2021-02-22 22:30:42
 *
 * @description 店铺管理Service接口
 */
public interface ShopService extends IService<Shop> {
    }
