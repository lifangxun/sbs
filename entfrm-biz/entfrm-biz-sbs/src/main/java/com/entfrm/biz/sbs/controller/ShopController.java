package com.entfrm.biz.sbs.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
        import com.baomidou.mybatisplus.core.metadata.IPage;
    import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
    import com.entfrm.biz.sbs.entity.Shop;
import com.entfrm.biz.sbs.service.ShopService;
import com.entfrm.core.log.annotation.OperLog;
import org.springframework.security.access.prepost.PreAuthorize;
import com.entfrm.core.base.api.R;
import com.entfrm.core.base.util.ExcelUtil;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Arrays;

/**
 * @author entfrm
 * @date 2021-02-22 22:30:42
 *
 * @description 店铺管理Controller
 */
@Api("店铺管理管理")
@RestController
@AllArgsConstructor
@RequestMapping("/sbs/shop")
public class ShopController {

    private final ShopService shopService;

    private QueryWrapper<Shop> getQueryWrapper(Shop shop) {
            return new QueryWrapper<Shop>()
                                                            .like(StrUtil.isNotBlank(shop.getShopName()), "shop_name", shop.getShopName())
                                                    .eq(StrUtil.isNotBlank(shop.getShopPlatform()), "shop_platform", shop.getShopPlatform())
                                                    .eq(StrUtil.isNotBlank(shop.getShopLevel()), "shop_level", shop.getShopLevel())
                                                    .eq(StrUtil.isNotBlank(shop.getStatus()), "status", shop.getStatus())
                                                    .eq(StrUtil.isNotBlank(shop.getShopUrl()), "shop_url", shop.getShopUrl())
                                                                                                                                                    .orderByDesc("create_time");
    }

            @ApiOperation("店铺管理列表")
        @PreAuthorize("@ps.hasPerm('shop_view')")
        @GetMapping("/list")
        public R list(Page page, Shop shop) {
            IPage<Shop> shopPage = shopService.page(page, getQueryWrapper(shop));
            return R.ok(shopPage.getRecords(), shopPage.getTotal());
        }
    
    @ApiOperation("店铺管理查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") Integer id) {
        return R.ok(shopService.getById(id));
    }

    @OperLog("店铺管理新增")
    @ApiOperation("店铺管理新增")
    @PreAuthorize("@ps.hasPerm('shop_add')")
    @PostMapping("/save")
    public R save(@Validated @RequestBody Shop shop) {
            shopService.save(shop);
        return R.ok();
    }

    @OperLog("店铺管理修改")
    @ApiOperation("店铺管理修改")
    @PreAuthorize("@ps.hasPerm('shop_edit')")
    @PutMapping("/update")
    public R update(@Validated @RequestBody Shop shop) {
            shopService.updateById(shop);
        return R.ok();
    }


    @OperLog("店铺管理删除")
    @ApiOperation("店铺管理删除")
    @PreAuthorize("@ps.hasPerm('shop_del')")
    @DeleteMapping("/remove/{id}")
    public R remove(@PathVariable("id") Integer[] id) {
        return R.ok(shopService.removeByIds(Arrays.asList(id)));
    }


    
    @PreAuthorize("@ps.hasPerm('shop_export')")
    @GetMapping("/export")
    public R export(Shop shop) {
        List<Shop> list = shopService.list(getQueryWrapper(shop));
        ExcelUtil<Shop> util = new ExcelUtil<Shop>(Shop. class);
        return util.exportExcel(list, "店铺管理数据");
    }
}
