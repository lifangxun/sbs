package com.entfrm.biz.sbs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.sbs.entity.Connect;

/**
 * @author entfrm
 * @date 2021-02-22 22:31:12
 *
 * @description 连接记录Mapper接口
 */
public interface ConnectMapper extends BaseMapper<Connect>{

}
