package com.entfrm.biz.sbs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.entfrm.core.base.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.entfrm.core.data.entity.BaseEntity;

/**
 * @author entfrm
 * @date 2021-02-22 22:30:42
 *
 * @description 店铺管理对象 Shop
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sbs_shop")
public class Shop extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /**  */
    @TableId
    private Integer id;

    /** 店铺名称 */
    @Excel(name = "店铺名称")
    private String shopName;

    /** 店铺账号 */
    @Excel(name = "店铺账号")
    private String shopAccount;

    /** 平台（淘宝，京东） */
    @Excel(name = "平台", convertExp = "淘=宝，京东")
    private String shopPlatform;

    /** 店铺等级 */
    @Excel(name = "店铺等级")
    private String shopLevel;

    /** 状态（是否审核） */
    @Excel(name = "状态", convertExp = "是=否审核")
    private String status;

    /** 店铺网址 */
    @Excel(name = "店铺网址")
    private String shopUrl;


}
