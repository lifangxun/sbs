package com.entfrm.biz.sbs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.sbs.entity.VipInfo;

/**
 * @author yy
 * @date 2021-01-30 15:06:27
 *
 * @description 会员信息Mapper接口
 */
public interface VipInfoMapper extends BaseMapper<VipInfo>{

}
