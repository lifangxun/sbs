package com.entfrm.biz.sbs.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.entfrm.biz.sbs.entity.OnhookList;
import com.entfrm.biz.sbs.service.OnhookListService;
import com.entfrm.core.log.annotation.OperLog;
import org.springframework.security.access.prepost.PreAuthorize;
import com.entfrm.core.base.api.R;
import com.entfrm.core.base.util.ExcelUtil;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
import java.util.Arrays;

/**
 * @author lfx&amp;yy
 * @date 2021-01-30 15:06:04
 * @description 挂机列表Controller
 */
@Api("挂机列表管理")
@RestController
@AllArgsConstructor
@RequestMapping("/sbs/onhookList")
public class OnhookListController {

    private final OnhookListService onhookListService;

    private QueryWrapper<OnhookList> getQueryWrapper(OnhookList onhookList) {
        return new QueryWrapper<OnhookList>()
                .eq(StrUtil.isNotBlank(onhookList.getHookPartition()), "hook_partition", onhookList.getHookPartition())
                .eq(!StrUtil.isEmptyIfStr(onhookList.getUserId()), "user_id", onhookList.getUserId())
                .eq(StrUtil.isNotBlank(onhookList.getTbTrumpet()), "tb_trumpet", onhookList.getTbTrumpet())
                .eq(StrUtil.isNotBlank(onhookList.getStarLevel()), "star_level", onhookList.getStarLevel())
                .eq(StrUtil.isNotBlank(onhookList.getConsumeLevel()), "consume_level", onhookList.getConsumeLevel())
                .eq(!StrUtil.isEmptyIfStr(onhookList.getDayBrush()), "day_brush", onhookList.getDayBrush())
                .eq(!StrUtil.isEmptyIfStr(onhookList.getMyBrush()), "my_brush", onhookList.getMyBrush())
                .eq(StrUtil.isNotBlank(onhookList.getStatus()), "status", onhookList.getStatus())
                .eq(StrUtil.isNotBlank(onhookList.getTag()), "tag", onhookList.getTag())
                .eq(StrUtil.isNotBlank(onhookList.getMaxMoney()), "max_money", onhookList.getMaxMoney())
                .eq(StrUtil.isNotBlank(onhookList.getAdvancePay()), "advance_pay", onhookList.getAdvancePay())
                .eq(StrUtil.isNotBlank(onhookList.getAddress()), "address", onhookList.getAddress())
                .eq(StrUtil.isNotBlank(onhookList.getNetwork()), "network", onhookList.getNetwork())
                .orderByDesc("create_time");
    }

    @ApiOperation("挂机列表列表")
    @PreAuthorize("@ps.hasPerm('onhookList_view')")
    @GetMapping("/list")
    public R list(Page page, OnhookList onhookList) {
        IPage<OnhookList> onhookListPage = onhookListService.page(page, getQueryWrapper(onhookList));
        return R.ok(onhookListPage.getRecords(), onhookListPage.getTotal());
    }

    @ApiOperation("挂机列表查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") Integer id) {
        return R.ok(onhookListService.getById(id));
    }

    @OperLog("挂机列表新增")
    @ApiOperation("挂机列表新增")
    @PreAuthorize("@ps.hasPerm('onhookList_add')")
    @PostMapping("/save")
    public R save(@Validated @RequestBody OnhookList onhookList) {
        onhookListService.save(onhookList);
        return R.ok();
    }

    @OperLog("挂机列表修改")
    @ApiOperation("挂机列表修改")
    @PreAuthorize("@ps.hasPerm('onhookList_edit')")
    @PutMapping("/update")
    public R update(@Validated @RequestBody OnhookList onhookList) {
        onhookListService.updateById(onhookList);
        return R.ok();
    }


    @OperLog("挂机列表删除")
    @ApiOperation("挂机列表删除")
    @PreAuthorize("@ps.hasPerm('onhookList_del')")
    @DeleteMapping("/remove/{id}")
    public R remove(@PathVariable("id") Integer[] id) {
        return R.ok(onhookListService.removeByIds(Arrays.asList(id)));
    }


    @PreAuthorize("@ps.hasPerm('onhookList_export')")
    @GetMapping("/export")
    public R export(OnhookList onhookList) {
        List<OnhookList> list = onhookListService.list(getQueryWrapper(onhookList));
        ExcelUtil<OnhookList> util = new ExcelUtil<OnhookList>(OnhookList.class);
        return util.exportExcel(list, "挂机列表数据");
    }
}
