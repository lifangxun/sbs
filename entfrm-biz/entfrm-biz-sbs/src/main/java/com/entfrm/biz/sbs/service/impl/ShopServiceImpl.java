package com.entfrm.biz.sbs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
    import org.springframework.stereotype.Service;
import com.entfrm.biz.sbs.mapper.ShopMapper;
import com.entfrm.biz.sbs.entity.Shop;
import com.entfrm.biz.sbs.service.ShopService;

/**
 * @author entfrm
 * @date 2021-02-22 22:30:42
 *
 * @description 店铺管理Service业务层
 */
@Service
public class ShopServiceImpl extends ServiceImpl<ShopMapper, Shop> implements ShopService {
    }
