package com.entfrm.biz.sbs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
    import org.springframework.stereotype.Service;
import com.entfrm.biz.sbs.mapper.BankInfoMapper;
import com.entfrm.biz.sbs.entity.BankInfo;
import com.entfrm.biz.sbs.service.BankInfoService;

/**
 * @author entfrm
 * @date 2021-02-23 23:17:49
 *
 * @description 银行列表Service业务层
 */
@Service
public class BankInfoServiceImpl extends ServiceImpl<BankInfoMapper, BankInfo> implements BankInfoService {
    }
