package com.entfrm.biz.sbs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
    import org.springframework.stereotype.Service;
import com.entfrm.biz.sbs.mapper.BlacklistMapper;
import com.entfrm.biz.sbs.entity.Blacklist;
import com.entfrm.biz.sbs.service.BlacklistService;

/**
 * @author entfrm
 * @date 2021-02-22 22:33:05
 *
 * @description 黑名单管理Service业务层
 */
@Service
public class BlacklistServiceImpl extends ServiceImpl<BlacklistMapper, Blacklist> implements BlacklistService {
    }
