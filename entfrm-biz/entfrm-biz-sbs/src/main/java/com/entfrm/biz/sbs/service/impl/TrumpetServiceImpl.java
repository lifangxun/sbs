package com.entfrm.biz.sbs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
    import org.springframework.stereotype.Service;
import com.entfrm.biz.sbs.mapper.TrumpetMapper;
import com.entfrm.biz.sbs.entity.Trumpet;
import com.entfrm.biz.sbs.service.TrumpetService;

/**
 * @author entfrm
 * @date 2021-02-22 23:16:44
 *
 * @description 小号管理Service业务层
 */
@Service
public class TrumpetServiceImpl extends ServiceImpl<TrumpetMapper, Trumpet> implements TrumpetService {
    }
