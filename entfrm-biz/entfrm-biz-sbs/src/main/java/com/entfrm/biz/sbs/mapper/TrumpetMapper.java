package com.entfrm.biz.sbs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.sbs.entity.Trumpet;

/**
 * @author entfrm
 * @date 2021-02-22 23:16:44
 *
 * @description 小号管理Mapper接口
 */
public interface TrumpetMapper extends BaseMapper<Trumpet>{

}
