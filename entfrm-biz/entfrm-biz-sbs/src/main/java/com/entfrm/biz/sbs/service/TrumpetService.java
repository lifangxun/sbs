package com.entfrm.biz.sbs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.sbs.entity.Trumpet;

/**
 * @author entfrm
 * @date 2021-02-22 23:16:44
 *
 * @description 小号管理Service接口
 */
public interface TrumpetService extends IService<Trumpet> {
    }
