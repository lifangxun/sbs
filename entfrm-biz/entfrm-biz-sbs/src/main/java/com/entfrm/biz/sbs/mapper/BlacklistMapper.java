package com.entfrm.biz.sbs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.sbs.entity.Blacklist;

/**
 * @author entfrm
 * @date 2021-02-22 22:33:05
 *
 * @description 黑名单管理Mapper接口
 */
public interface BlacklistMapper extends BaseMapper<Blacklist>{

}
