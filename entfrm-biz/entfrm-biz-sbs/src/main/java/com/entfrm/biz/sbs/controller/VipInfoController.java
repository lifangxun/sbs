package com.entfrm.biz.sbs.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.entfrm.biz.sbs.entity.VipInfo;
import com.entfrm.biz.sbs.service.VipInfoService;
import com.entfrm.core.log.annotation.OperLog;
import org.springframework.security.access.prepost.PreAuthorize;
import com.entfrm.core.base.api.R;
import com.entfrm.core.base.util.ExcelUtil;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
import java.util.Arrays;

/**
 * @author yy
 * @date 2021-01-30 15:06:27
 * @description 会员信息Controller
 */
@Api("会员信息管理")
@RestController
@AllArgsConstructor
@RequestMapping("/sbs/vipInfo")
public class VipInfoController {

    private final VipInfoService vipInfoService;

    private QueryWrapper<VipInfo> getQueryWrapper(VipInfo vipInfo) {
        return new QueryWrapper<VipInfo>()
                .eq(!StrUtil.isEmptyIfStr(vipInfo.getUserId()), "user_id", vipInfo.getUserId())
                .like(StrUtil.isNotBlank(vipInfo.getUserName()), "user_name", vipInfo.getUserName())
                .eq(StrUtil.isNotBlank(vipInfo.getVipType()), "vip_type", vipInfo.getVipType())
                .eq(StrUtil.isNotBlank(vipInfo.getBalance()), "balance", vipInfo.getBalance())
                .eq(StrUtil.isNotBlank(vipInfo.getGoldCoin()), "gold_coin", vipInfo.getGoldCoin())
                .eq(StrUtil.isNotBlank(vipInfo.getCommission()), "commission", vipInfo.getCommission())
                .eq(StrUtil.isNotBlank(vipInfo.getStatus()), "status", vipInfo.getStatus())
                .eq(StrUtil.isNotBlank(vipInfo.getSex()), "sex", vipInfo.getSex())
                .orderByDesc("create_time");
    }

    @ApiOperation("会员信息列表")
    @PreAuthorize("@ps.hasPerm('vipInfo_view')")
    @GetMapping("/list")
    public R list(Page page, VipInfo vipInfo) {
        IPage<VipInfo> vipInfoPage = vipInfoService.page(page, getQueryWrapper(vipInfo));
        return R.ok(vipInfoPage.getRecords(), vipInfoPage.getTotal());
    }

    @ApiOperation("会员信息查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") Integer id) {
        return R.ok(vipInfoService.getById(id));
    }

    @OperLog("会员信息新增")
    @ApiOperation("会员信息新增")
    @PreAuthorize("@ps.hasPerm('vipInfo_add')")
    @PostMapping("/save")
    public R save(@Validated @RequestBody VipInfo vipInfo) {
        vipInfoService.save(vipInfo);
        return R.ok();
    }

    @OperLog("会员信息修改")
    @ApiOperation("会员信息修改")
    @PreAuthorize("@ps.hasPerm('vipInfo_edit')")
    @PutMapping("/update")
    public R update(@Validated @RequestBody VipInfo vipInfo) {
        vipInfoService.updateById(vipInfo);
        return R.ok();
    }


    @OperLog("会员信息删除")
    @ApiOperation("会员信息删除")
    @PreAuthorize("@ps.hasPerm('vipInfo_del')")
    @DeleteMapping("/remove/{id}")
    public R remove(@PathVariable("id") Integer[] id) {
        return R.ok(vipInfoService.removeByIds(Arrays.asList(id)));
    }


    @PreAuthorize("@ps.hasPerm('vipInfo_export')")
    @GetMapping("/export")
    public R export(VipInfo vipInfo) {
        List<VipInfo> list = vipInfoService.list(getQueryWrapper(vipInfo));
        ExcelUtil<VipInfo> util = new ExcelUtil<VipInfo>(VipInfo.class);
        return util.exportExcel(list, "会员信息数据");
    }
}
