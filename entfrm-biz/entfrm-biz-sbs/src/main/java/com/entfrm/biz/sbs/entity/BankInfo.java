package com.entfrm.biz.sbs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.entfrm.core.base.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.entfrm.core.data.entity.BaseEntity;

/**
 * @author entfrm
 * @date 2021-02-23 23:17:49
 *
 * @description 银行列表对象 BankInfo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sbs_bank_info")
public class BankInfo extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /**  */
    @TableId
    private Integer id;

    /** 收款人姓名 */
    @Excel(name = "收款人姓名")
    private String accountName;

    /** 账号类型 */
    @Excel(name = "账号类型")
    private String accountType;

    /** 会员信息 */
    @Excel(name = "会员信息")
    private Integer userId;

    /** 银行卡账号 */
    @Excel(name = "银行卡账号")
    private String cardNo;

    /** 开户行 */
    @Excel(name = "开户行")
    private String bankType;

    /** 开户银行地址 */
    @Excel(name = "开户银行地址")
    private String bankAddress;

    /** 绑定手机 */
    @Excel(name = "绑定手机")
    private String phone;

    /** QQ号 */
    @Excel(name = "QQ号")
    private String qq;


}
