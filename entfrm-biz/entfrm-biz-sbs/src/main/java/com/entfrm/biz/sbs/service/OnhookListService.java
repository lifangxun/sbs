package com.entfrm.biz.sbs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.sbs.entity.OnhookList;

/**
 * @author lfx&amp;yy
 * @date 2021-01-30 15:06:04
 *
 * @description 挂机列表Service接口
 */
public interface OnhookListService extends IService<OnhookList> {
    }
