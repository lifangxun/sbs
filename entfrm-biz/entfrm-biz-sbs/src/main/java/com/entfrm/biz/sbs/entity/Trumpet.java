package com.entfrm.biz.sbs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.entfrm.core.base.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.entfrm.core.data.entity.BaseEntity;

/**
 * @author entfrm
 * @date 2021-02-22 23:16:44
 *
 * @description 小号管理对象 Trumpet
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sbs_trumpet")
public class Trumpet extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /**  */
    @TableId
    private Integer id;

    /** 大号id */
    @Excel(name = "大号id")
    private Integer userId;

    /** 账号名 */
    @Excel(name = "账号名")
    private String trumpetName;

    /** 账号密码 */
    @Excel(name = "账号密码")
    private String trumpetPwd;

    /** 账号等级 */
    @Excel(name = "账号等级")
    private String trumpetLevel;

    /** 账号平台 */
    @Excel(name = "账号平台")
    private String trumpetPlateform;

    /** 消费层级 */
    @Excel(name = "消费层级")
    private String consumeLevel;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 年龄 */
    @Excel(name = "年龄")
    private String age;

    /** 支付宝姓名 */
    @Excel(name = "支付宝姓名")
    private String zfbName;

    /** 支付宝账号 */
    @Excel(name = "支付宝账号")
    private String zfbAccount;

    /** 收货人姓名 */
    @Excel(name = "收货人姓名")
    private String receiveName;

    /** 收货人电话 */
    @Excel(name = "收货人电话")
    private String receivePhone;

    /** 收货地址 */
    @Excel(name = "收货地址")
    private String receiveAddress;

    /** 是否实名 */
    @Excel(name = "是否实名")
    private String isRealname;

    /** 是否手机实名 */
    @Excel(name = "是否手机实名")
    private String isRealphone;

    /** 是否启用 */
    @Excel(name = "是否启用")
    private String isOn;

    /** 是否审核 */
    @Excel(name = "是否审核")
    private String isAudit;

    /** 上限订单数 */
    @Excel(name = "上限订单数")
    private String maxOrderNo;

    /** 上限金额 */
    @Excel(name = "上限金额")
    private String maxMoney;

    /** 状态（连接，空闲） */
    @Excel(name = "状态", convertExp = "连=接，空闲")
    private String status;


}
