package com.entfrm.biz.sbs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.sbs.entity.BankInfo;

/**
 * @author entfrm
 * @date 2021-02-23 23:17:49
 *
 * @description 银行列表Mapper接口
 */
public interface BankInfoMapper extends BaseMapper<BankInfo>{

}
