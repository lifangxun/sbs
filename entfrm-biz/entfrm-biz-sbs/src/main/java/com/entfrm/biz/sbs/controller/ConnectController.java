package com.entfrm.biz.sbs.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.entfrm.biz.sbs.entity.Connect;
import com.entfrm.biz.sbs.service.ConnectService;
import com.entfrm.core.log.annotation.OperLog;
import org.springframework.security.access.prepost.PreAuthorize;
import com.entfrm.core.base.api.R;
import com.entfrm.core.base.util.ExcelUtil;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
import java.util.Arrays;

/**
 * @author entfrm
 * @date 2021-02-22 22:31:12
 * @description 连接记录Controller
 */
@Api("连接记录管理")
@RestController
@AllArgsConstructor
@RequestMapping("/sbs/connect")
public class ConnectController {

    private final ConnectService connectService;

    private QueryWrapper<Connect> getQueryWrapper(Connect connect) {
        return new QueryWrapper<Connect>()
                .eq(StrUtil.isNotBlank(connect.getAddress()), "address", connect.getAddress())
                .eq(!StrUtil.isEmptyIfStr(connect.getConnectId()), "connect_id", connect.getConnectId())
                .eq(!StrUtil.isEmptyIfStr(connect.getConnectById()), "connect_by_id", connect.getConnectById())
                .orderByDesc("create_time");
    }

    @ApiOperation("连接记录列表")
    @PreAuthorize("@ps.hasPerm('connect_view')")
    @GetMapping("/list")
    public R list(Page page, Connect connect) {
        IPage<Connect> connectPage = connectService.page(page, getQueryWrapper(connect));
        return R.ok(connectPage.getRecords(), connectPage.getTotal());
    }

    @ApiOperation("连接记录查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") Integer id) {
        return R.ok(connectService.getById(id));
    }

    @OperLog("连接记录新增")
    @ApiOperation("连接记录新增")
    @PreAuthorize("@ps.hasPerm('connect_add')")
    @PostMapping("/save")
    public R save(@Validated @RequestBody Connect connect) {
        connectService.save(connect);
        return R.ok();
    }

    @OperLog("连接记录修改")
    @ApiOperation("连接记录修改")
    @PreAuthorize("@ps.hasPerm('connect_edit')")
    @PutMapping("/update")
    public R update(@Validated @RequestBody Connect connect) {
        connectService.updateById(connect);
        return R.ok();
    }


    @OperLog("连接记录删除")
    @ApiOperation("连接记录删除")
    @PreAuthorize("@ps.hasPerm('connect_del')")
    @DeleteMapping("/remove/{id}")
    public R remove(@PathVariable("id") Integer[] id) {
        return R.ok(connectService.removeByIds(Arrays.asList(id)));
    }


    @PreAuthorize("@ps.hasPerm('connect_export')")
    @GetMapping("/export")
    public R export(Connect connect) {
        List<Connect> list = connectService.list(getQueryWrapper(connect));
        ExcelUtil<Connect> util = new ExcelUtil<Connect>(Connect.class);
        return util.exportExcel(list, "连接记录数据");
    }
}
