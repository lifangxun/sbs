package com.entfrm.biz.sbs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.sbs.entity.Connect;

/**
 * @author entfrm
 * @date 2021-02-22 22:31:12
 *
 * @description 连接记录Service接口
 */
public interface ConnectService extends IService<Connect> {
    }
