package com.entfrm.biz.sbs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
    import org.springframework.stereotype.Service;
import com.entfrm.biz.sbs.mapper.VipInfoMapper;
import com.entfrm.biz.sbs.entity.VipInfo;
import com.entfrm.biz.sbs.service.VipInfoService;

/**
 * @author yy
 * @date 2021-01-30 15:06:27
 *
 * @description 会员信息Service业务层
 */
@Service
public class VipInfoServiceImpl extends ServiceImpl<VipInfoMapper, VipInfo> implements VipInfoService {
    }
