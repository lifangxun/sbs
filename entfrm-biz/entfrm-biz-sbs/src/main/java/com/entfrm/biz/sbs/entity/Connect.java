package com.entfrm.biz.sbs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.entfrm.core.base.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.entfrm.core.data.entity.BaseEntity;
import java.util.Date;

/**
 * @author entfrm
 * @date 2021-02-22 22:31:12
 *
 * @description 连接记录对象 Connect
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sbs_connect")
public class Connect extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /**  */
    @TableId
    private Integer id;

    /** 地址  */
    @Excel(name = "地址 ")
    private String address;

    /** 连接时长 */
    @Excel(name = "连接时长")
    private String connectTime;

    /** 连接时间 */
    @Excel(name = "连接时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date connectDate;

    /** 连接人 */
    @Excel(name = "连接人")
    private Integer connectId;

    /** 被连接人 */
    @Excel(name = "被连接人")
    private Integer connectById;


}
