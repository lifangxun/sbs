package com.entfrm.biz.devtool.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.devtool.entity.Table;


/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author entfrm
 * @since 2019-01-30
 */
public interface TableMapper extends BaseMapper<Table> {

}
