package com.entfrm.biz.devtool.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.devtool.entity.Apiinfo;

/**
 * @author entfrm
 * @date 2020-04-24 22:18:00
 * @description 接口Service接口
 */
public interface ApiinfoService extends IService<Apiinfo> {
}
