package com.entfrm.biz.msg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.msg.entity.InfoTemplate;

/**
 * @author entfrm
 * @date 2020-05-24 22:26:58
 * @description 消息模板Service接口
 */
public interface InfoTemplateService extends IService<InfoTemplate> {
}
