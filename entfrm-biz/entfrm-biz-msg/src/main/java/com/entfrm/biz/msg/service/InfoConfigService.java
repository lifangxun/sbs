package com.entfrm.biz.msg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.msg.entity.InfoConfig;

/**
 * @author entfrm
 * @date 2020-05-10 14:53:43
 * @description 消息配置Service接口
 */
public interface InfoConfigService extends IService<InfoConfig> {
}
