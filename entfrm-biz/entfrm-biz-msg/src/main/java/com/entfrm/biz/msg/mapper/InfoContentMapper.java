package com.entfrm.biz.msg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.msg.entity.InfoContent;

/**
 * @author entfrm
 * @date 2020-05-23 12:14:03
 *
 * @description 消息内容Mapper接口
 */
public interface InfoContentMapper extends BaseMapper<InfoContent>{

}
