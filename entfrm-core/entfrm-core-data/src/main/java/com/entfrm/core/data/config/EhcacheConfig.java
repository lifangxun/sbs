package com.entfrm.core.data.config;

import lombok.extern.slf4j.Slf4j;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

/**
 * @author tumengtech
 * @date 2020/7/2
 * @description ehcache 缓存
 */
@Configuration
@Slf4j
public class EhcacheConfig {

    @Bean
    @ConditionalOnMissingBean
    public CacheManager cacheManager() {
        CacheManagerBuilder cacheManagerBuilder = CacheManagerBuilder.newCacheManagerBuilder().withCache("sysCache",
                CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class,
                        //设置缓存堆容纳元素个数(JVM内存空间)超出个数后会存到offheap中 设置堆外储存大小(内存存储) 超出offheap的大小会淘汰规则被淘汰
                        ResourcePoolsBuilder.heap(100L).offheap(100L, MemoryUnit.MB))
                        //设置缓存过期时间
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofMinutes(30L)))
                        //设置被访问后过期时间(同时设置和TTL和TTI之后会被覆盖,这里TTI生效,之前版本xml配置后是两个配置了都会生效)
                        .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofMinutes(60L))).build()).withCache("captchaCache",
                CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class,
                        //设置缓存堆容纳元素个数(JVM内存空间)超出个数后会存到offheap中 设置堆外储存大小(内存存储) 超出offheap的大小会淘汰规则被淘汰
                        ResourcePoolsBuilder.heap(100L).offheap(50L, MemoryUnit.MB))
                        //设置缓存过期时间
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(180L)))
                        //设置被访问后过期时间(同时设置和TTL和TTI之后会被覆盖,这里TTI生效,之前版本xml配置后是两个配置了都会生效)
                        .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofSeconds(120L))).build());

        CacheManager cacheManager = cacheManagerBuilder.build(true);
        Cache<String, String> sysCache = cacheManager.getCache("sysCache", String.class, String.class);
        if (sysCache == null) {
            log.error("sysCache 初始化异常");
        }
        Cache<String, String> captchaCache = cacheManager.getCache("captchaCache", String.class, String.class);
        if (captchaCache == null) {
            log.error("sysCache 初始化异常");
        }
        return cacheManager;
    }
}
