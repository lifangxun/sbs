import request from '@/utils/request'

// 查询示例列表
export function listDemo(query) {
  return request({
    url: '/cms/demo/list',
    method: 'get',
    params: query
  })
}

// 查询示例详细
export function getDemo(id) {
  return request({
    url: '/cms/demo/' + id,
    method: 'get'
  })
}

// 新增示例
export function addDemo(data) {
  return request({
    url: '/cms/demo/save',
    method: 'post',
    data: data
  })
}

// 修改示例
export function editDemo(data) {
  return request({
    url: '/cms/demo/update',
    method: 'put',
    data: data
  })
}

// 删除示例
export function delDemo(id) {
  return request({
    url: '/cms/demo/remove/' + id,
    method: 'delete'
  })
}



// 导出示例
export function exportDemo(query) {
  return request({
    url: '/cms/demo/export',
    method: 'get',
    params: query
  })
}
