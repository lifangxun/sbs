import request from '@/utils/request'

// 查询银行列表列表
export function listBankInfo(query) {
  return request({
    url: '/sbs/bankInfo/list',
    method: 'get',
    params: query
  })
}

// 查询银行列表详细
export function getBankInfo(id) {
  return request({
    url: '/sbs/bankInfo/' + id,
    method: 'get'
  })
}

// 新增银行列表
export function addBankInfo(data) {
  return request({
    url: '/sbs/bankInfo/save',
    method: 'post',
    data: data
  })
}

// 修改银行列表
export function editBankInfo(data) {
  return request({
    url: '/sbs/bankInfo/update',
    method: 'put',
    data: data
  })
}

// 删除银行列表
export function delBankInfo(id) {
  return request({
    url: '/sbs/bankInfo/remove/' + id,
    method: 'delete'
  })
}



// 导出银行列表
export function exportBankInfo(query) {
  return request({
    url: '/sbs/bankInfo/export',
    method: 'get',
    params: query
  })
}
