import request from '@/utils/request'

// 查询会员信息列表
export function listVipInfo(query) {
  return request({
    url: '/sbs/vipInfo/list',
    method: 'get',
    params: query
  })
}

// 查询会员信息详细
export function getVipInfo(id) {
  return request({
    url: '/sbs/vipInfo/' + id,
    method: 'get'
  })
}

// 新增会员信息
export function addVipInfo(data) {
  return request({
    url: '/sbs/vipInfo/save',
    method: 'post',
    data: data
  })
}

// 修改会员信息
export function editVipInfo(data) {
  return request({
    url: '/sbs/vipInfo/update',
    method: 'put',
    data: data
  })
}

// 删除会员信息
export function delVipInfo(id) {
  return request({
    url: '/sbs/vipInfo/remove/' + id,
    method: 'delete'
  })
}



// 导出会员信息
export function exportVipInfo(query) {
  return request({
    url: '/sbs/vipInfo/export',
    method: 'get',
    params: query
  })
}
