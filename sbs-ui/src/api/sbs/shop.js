import request from '@/utils/request'

// 查询店铺管理列表
export function listShop(query) {
  return request({
    url: '/sbs/shop/list',
    method: 'get',
    params: query
  })
}

// 查询店铺管理详细
export function getShop(id) {
  return request({
    url: '/sbs/shop/' + id,
    method: 'get'
  })
}

// 新增店铺管理
export function addShop(data) {
  return request({
    url: '/sbs/shop/save',
    method: 'post',
    data: data
  })
}

// 修改店铺管理
export function editShop(data) {
  return request({
    url: '/sbs/shop/update',
    method: 'put',
    data: data
  })
}

// 删除店铺管理
export function delShop(id) {
  return request({
    url: '/sbs/shop/remove/' + id,
    method: 'delete'
  })
}



// 导出店铺管理
export function exportShop(query) {
  return request({
    url: '/sbs/shop/export',
    method: 'get',
    params: query
  })
}
