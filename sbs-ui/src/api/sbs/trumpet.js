import request from '@/utils/request'

// 查询小号管理列表
export function listTrumpet(query) {
  return request({
    url: '/sbs/trumpet/list',
    method: 'get',
    params: query
  })
}

// 查询小号管理详细
export function getTrumpet(id) {
  return request({
    url: '/sbs/trumpet/' + id,
    method: 'get'
  })
}

// 新增小号管理
export function addTrumpet(data) {
  return request({
    url: '/sbs/trumpet/save',
    method: 'post',
    data: data
  })
}

// 修改小号管理
export function editTrumpet(data) {
  return request({
    url: '/sbs/trumpet/update',
    method: 'put',
    data: data
  })
}

// 删除小号管理
export function delTrumpet(id) {
  return request({
    url: '/sbs/trumpet/remove/' + id,
    method: 'delete'
  })
}



// 导出小号管理
export function exportTrumpet(query) {
  return request({
    url: '/sbs/trumpet/export',
    method: 'get',
    params: query
  })
}
