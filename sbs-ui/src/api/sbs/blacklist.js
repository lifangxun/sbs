import request from '@/utils/request'

// 查询黑名单管理列表
export function listBlacklist(query) {
  return request({
    url: '/sbs/blacklist/list',
    method: 'get',
    params: query
  })
}

// 查询黑名单管理详细
export function getBlacklist(id) {
  return request({
    url: '/sbs/blacklist/' + id,
    method: 'get'
  })
}

// 新增黑名单管理
export function addBlacklist(data) {
  return request({
    url: '/sbs/blacklist/save',
    method: 'post',
    data: data
  })
}

// 修改黑名单管理
export function editBlacklist(data) {
  return request({
    url: '/sbs/blacklist/update',
    method: 'put',
    data: data
  })
}

// 删除黑名单管理
export function delBlacklist(id) {
  return request({
    url: '/sbs/blacklist/remove/' + id,
    method: 'delete'
  })
}



// 导出黑名单管理
export function exportBlacklist(query) {
  return request({
    url: '/sbs/blacklist/export',
    method: 'get',
    params: query
  })
}
