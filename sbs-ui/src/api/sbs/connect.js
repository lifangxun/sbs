import request from '@/utils/request'

// 查询连接记录列表
export function listConnect(query) {
  return request({
    url: '/sbs/connect/list',
    method: 'get',
    params: query
  })
}

// 查询连接记录详细
export function getConnect(id) {
  return request({
    url: '/sbs/connect/' + id,
    method: 'get'
  })
}

// 新增连接记录
export function addConnect(data) {
  return request({
    url: '/sbs/connect/save',
    method: 'post',
    data: data
  })
}

// 修改连接记录
export function editConnect(data) {
  return request({
    url: '/sbs/connect/update',
    method: 'put',
    data: data
  })
}

// 删除连接记录
export function delConnect(id) {
  return request({
    url: '/sbs/connect/remove/' + id,
    method: 'delete'
  })
}



// 导出连接记录
export function exportConnect(query) {
  return request({
    url: '/sbs/connect/export',
    method: 'get',
    params: query
  })
}
