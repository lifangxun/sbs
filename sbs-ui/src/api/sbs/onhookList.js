import request from '@/utils/request'

// 查询挂机列表列表
export function listOnhookList(query) {
  return request({
    url: '/sbs/onhookList/list',
    method: 'get',
    params: query
  })
}

// 查询挂机列表详细
export function getOnhookList(id) {
  return request({
    url: '/sbs/onhookList/' + id,
    method: 'get'
  })
}

// 新增挂机列表
export function addOnhookList(data) {
  return request({
    url: '/sbs/onhookList/save',
    method: 'post',
    data: data
  })
}

// 修改挂机列表
export function editOnhookList(data) {
  return request({
    url: '/sbs/onhookList/update',
    method: 'put',
    data: data
  })
}

// 删除挂机列表
export function delOnhookList(id) {
  return request({
    url: '/sbs/onhookList/remove/' + id,
    method: 'delete'
  })
}



// 导出挂机列表
export function exportOnhookList(query) {
  return request({
    url: '/sbs/onhookList/export',
    method: 'get',
    params: query
  })
}
